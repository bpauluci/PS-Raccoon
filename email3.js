var cart_products = [{
    'product_id': 'IN2350',
    'product_name': 'Teclado Mecânico Rozar K600',
    'product_price': '650.00',
    'product_quantity': '1',
    'product_url': 'http://www.foo.com/p/IN2350',
    'product_category': 'peripherals',
    'product_brand': 'Rozar'
}, {
    'product_id': 'IN4566',
    'product_name': 'Monitor Gamer LED GL UltraWide 25"',
    'product_price': '1399.99',
    'product_quantity': '1',
    'product_url': 'http://www.foo.com/p/IN4566',
    'product_category': 'monitor',
    'product_brand': 'GL'
}, {
    'product_id': 'ES7112',
    'product_name': 'Pacote de Post-it Fenix 50 unidades',
    'product_price': '14.95',
    'product_quantity': '5',
    'product_url': 'http://www.foo.com/p/ES7112',
    'product_category': 'office',
    'product_brand': 'Fenix'
}, {
    'product_id': 'BR8810',
    'product_name': 'Miniatura Yoshi Haras',
    'product_price': '6.50',
    'product_quantity': '2',
    'product_url': 'http://www.foo.com/p/BR8810',
    'product_category': 'others',
    'product_brand': 'Haras'
}];
//Utilizo o método reduce() para escrever na variável tInfo a soma dos preços
//dos produtos multiplicados por sua respectiva quantidade no carrinho.
var tInfo = cart_products.reduce(function(acumulador, atual){
	return acumulador + atual.product_price * atual.product_quantity;
}, 0);
//Utilizo uma função que analisará o valor de tInfo, utilizando
//seu valor para classificar o atributo freeShipping como true ou false
//e criar o objeto pedido
function montaResposta (numero){
	return {totalPrice: numero, freeShipping: numero>300? true : false}
}
//Chamo a função que monta o objeto pedido na variável desejada
var cartPricesInfo = montaResposta(tInfo);

console.log(cartPricesInfo);
console.log(`totalPrice: ${cartPricesInfo.totalPrice}`);
console.log(`freeShipping: ${cartPricesInfo.freeShipping}`);

