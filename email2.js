var products_list = [
  {
    "id": "35",
    "name": "Miniatura Homem de Ferro",
    "price": "R$ 50.00"
  },
  {
    "id": "30",
    "name": "Miniatura Magneto",
    "price": "R$ 57.00"
  },{
    "id": "25",
    "name": "Miniatura Super Homem",
    "price": "R$ 37.00"
  },{
    "id": "71",
    "name": "Miniatura Bernard",
    "price": "R$ 71.00"
  },{
    "id": "56",
    "name": "Miniatura Batman",
    "price": "R$ 23.00"
  },
  {
    "id": "56",
    "name": "Miniatura Darth Vader",
    "price": "R$ 89.00"
  }];

function toNumber(item){ //A função que pega o valor "price" em string e o 
	var item_aux = Object.assign({}, item); //transforma em valor inteiro.
	var aux=item.price[2]; 
	for (var i=3; i<item.price.length; i++){
		aux+=item.price[i];
	}
	item_aux.price = parseInt(aux, 10);
	return item_aux;
}
//A função funciona criando uma variável auxiliar e escrevendo nela todos os números
//do atributo price. Os números são obtidos olhando nas entradas após o "R$" da string.
//Inicialmente, ia deixar o número como float, mas utilizei a função parseInt() para ficar
//semelhante à saída solicitada.

var new_products_list = products_list.map(toNumber);//Aqui, utilizo o método map() para 
    //escrever na variável new_products_list a saída pedida.

console.log(new_products_list);
